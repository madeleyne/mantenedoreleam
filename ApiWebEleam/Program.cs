﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ApiWebEleam
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }


        //habilitar cuando se este en Development environment ademas en properties cambiar el depurar a "Development" (cuando se necesiten crear nuevos controladores)
        /*
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>(); */


        //habilitar cuando se necesite publicar en IIS - ademas en properties cambiar el depurar a "production"

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>                                       
                
            WebHost.CreateDefaultBuilder(args)
               .UseIISIntegration()
               .UseKestrel()
               .UseContentRoot(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
               .ConfigureAppConfiguration((hostingContext, config) =>
               {
                   IHostingEnvironment env = hostingContext.HostingEnvironment;
                   config.SetBasePath(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
                   config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                   .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

               })
            .UseStartup<Startup>();
            


    }
}

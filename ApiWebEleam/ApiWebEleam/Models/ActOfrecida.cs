﻿using System;
using System.Collections.Generic;

namespace ApiWebEleam.Models
{
    public partial class ActOfrecida
    {
        public int IdactOfrecida { get; set; }
        public string NombreActOfrecida { get; set; }
        public string DescripActOfrecida { get; set; }
        public string AprobarActOfrecida { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ApiWebEleam.Models
{
    public partial class Registro
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string ApPaterno { get; set; }
        public string ApMaterno { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public string RepPassword { get; set; }
        public string ImagenPerfil { get; set; }
    }
}

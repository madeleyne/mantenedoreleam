﻿using System;
using System.Collections.Generic;

namespace ApiWebEleam.Models
{
    public partial class Actividades
    {
        public int Idactividades { get; set; }
        public string NombreActividad { get; set; }
        public string DescripActividad { get; set; }
        public string ImagenActividad { get; set; }
    }
}

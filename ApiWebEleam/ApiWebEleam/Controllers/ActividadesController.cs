﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiWebEleam.Models;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace ApiWebEleam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActividadesController : ControllerBase
    {
        private readonly eleamcruddbContext _context;
        public IHostingEnvironment Env { get; set; }

        public ActividadesController(eleamcruddbContext context, IHostingEnvironment environment)
        {
            _context = context;
            Env = environment;
        }

        // GET: api/Actividades
        [HttpGet]
        public IEnumerable<Actividades> GetActividades()
        {
            return _context.Actividades;
        }

        // GET: api/Actividades/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetActividades([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actividades = await _context.Actividades.FindAsync(id);

            if (actividades == null)
            {
                return NotFound();
            }

            return Ok(actividades);
        }



        // PUT: api/Actividades/5  [HttpPut("{id}")]
        [HttpPut]
        public async Task<IActionResult> PutActividades( [FromForm] string actividades, IFormFile imagenActividad)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Deserializar Json
            Actividades _actividades = JsonConvert.DeserializeObject<Actividades>(actividades);

            try
            {
                //Manejo de Excepciones
                if (string.IsNullOrEmpty(_actividades.NombreActividad))
                    throw new ArgumentException("Debe ingresar un nombre para la actividad");
                if (string.IsNullOrEmpty(_actividades.DescripActividad))
                    throw new ArgumentException("Debe ingresar una breve descripción");
                //if (_context.Actividades.Any(x => x.NombreActividad == _actividades.NombreActividad))
                    //throw new ArgumentException("Esta actividad ya existe");
                if (imagenActividad.Length <= 0)
                    throw new ArgumentException("No haz seleccionado ninguna imagen");
                if (imagenActividad.ContentType != "image/jpeg")
                    throw new ArgumentException("Solo se admiten imagenes en formato .jpg");
                if (imagenActividad.Length / 1000000 > 2)
                    throw new ArgumentException("El peso de la imagen debe menor a 2Gb");

                //guardar nombre de la imagen con un número único
                _actividades.ImagenActividad = Guid.NewGuid() + imagenActividad.FileName;

                _context.Actividades.Add(_actividades);
                await _context.SaveChangesAsync();

                //variable para obtener el registro que viene de la base de datos a partir del nombre de la actividad
                var actividadbd = _context.Actividades.FirstOrDefault(x => x.NombreActividad == _actividades.NombreActividad);


                //variable para guardar ruta de carpeta Imagen_Actividades, donde quedaran todas la imagenes de las actividades creadas

                var Ruta = Path.Combine(Env.WebRootPath, "Imagen_Actividades");

                //validación para ver si la carpeta existe y de no existir crearla

                if (!Directory.Exists(Ruta))
                {
                    Directory.CreateDirectory(Ruta);
                }

                using (var stream = new FileStream(Path.Combine(Ruta, _actividades.ImagenActividad), FileMode.Create))
                {
                    //esperar que copie la imagen en el nuevo directorio
                    await imagenActividad.CopyToAsync(stream);

                }

                return CreatedAtAction("GetActividades", new { id = actividadbd.Idactividades }, actividadbd);

            }
            catch (Exception ex)
            {

                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);

            }



        }

        // POST: api/Actividades
        [HttpPost]
        public async Task<IActionResult> PostActividades([FromForm] string actividades, IFormFile imagenActividad )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Deserializar Json
            Actividades _actividades = JsonConvert.DeserializeObject<Actividades>(actividades);

            try
            {
                //Manejo de Excepciones
                if (string.IsNullOrEmpty(_actividades.NombreActividad))
                    throw new ArgumentException("Debe ingresar un nombre para la actividad");
                if (string.IsNullOrEmpty(_actividades.DescripActividad))
                    throw new ArgumentException("Debe ingresar una breve descripción");
                if (_context.Actividades.Any(x => x.NombreActividad == _actividades.NombreActividad))
                    throw new ArgumentException("Esta actividad ya existe");                    
                if (imagenActividad.Length <= 0)
                    throw new ArgumentException("No haz seleccionado ninguna imagen");
                if (imagenActividad.ContentType != "image/jpeg")
                    throw new ArgumentException("Solo se admiten imagenes en formato .jpg");
                if (imagenActividad.Length / 1000000 > 2)
                    throw new ArgumentException("El peso de la imagen debe menor a 2Gb");

                //guardar nombre de la imagen con un número único
                _actividades.ImagenActividad = Guid.NewGuid() + imagenActividad.FileName;

                _context.Actividades.Add(_actividades);
                await _context.SaveChangesAsync();

                //variable para obtener el registro que viene de la base de datos a partir del nombre de la actividad
                var actividadbd = _context.Actividades.FirstOrDefault(x => x.NombreActividad == _actividades.NombreActividad);


                //variable para guardar ruta de carpeta Imagen_Actividades, donde quedaran todas la imagenes de las actividades creadas

                var Ruta = Path.Combine(Env.WebRootPath, "Imagen_Actividades");

                //validación para ver si la carpeta existe y de no existir crearla

                if (!Directory.Exists(Ruta))
                {
                    Directory.CreateDirectory(Ruta);
                }

                using (var stream = new FileStream(Path.Combine(Ruta, _actividades.ImagenActividad), FileMode.Create))
                {
                    //esperar que copie la imagen en el nuevo directorio
                    await imagenActividad.CopyToAsync(stream);

                }

                return CreatedAtAction("GetActividades", new { id = actividadbd.Idactividades }, actividadbd);

            }
            catch (Exception ex) {

                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);

            }

            
        }

        // DELETE: api/Actividades/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActividades([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actividades = await _context.Actividades.FindAsync(id);
            if (actividades == null)
            {
                return NotFound();
            }

            _context.Actividades.Remove(actividades);
            await _context.SaveChangesAsync();

            return Ok(actividades);
        }

        private bool ActividadesExists(int id)
        {
            return _context.Actividades.Any(e => e.Idactividades == id);
        }
    }
}
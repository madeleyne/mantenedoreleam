﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiWebEleam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        public IHostingEnvironment Env { get; set; }
        public ImageController(IHostingEnvironment environment)
        {
            Env = environment;
        }

        [HttpGet("{URL}")]

        public FileStreamResult GetImage(string URL) {

            var ruta = Path.Combine(Env.WebRootPath, "Imagen_Actividades");
            var stream = System.IO.File.OpenRead(Path.Combine(ruta, URL));

            return File(stream, "image/jpeg");
         


        }



    }

}
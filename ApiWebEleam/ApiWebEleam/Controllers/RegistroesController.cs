﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiWebEleam.Models;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace ApiWebEleam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroesController : ControllerBase
    {
        private readonly eleamcruddbContext _context;
        public IHostingEnvironment Env { get; set; }

        public RegistroesController(eleamcruddbContext context, IHostingEnvironment environment)
        {
            _context = context;
            Env = environment;
        }

        // GET: api/Registroes
        [HttpGet]
        public IEnumerable<Registro> GetRegistro()
        {
            return _context.Registro;
        }

        // GET: api/Registroes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRegistro([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var registro = await _context.Registro.FindAsync(id);

            if (registro == null)
            {
                return NotFound();
            }

            return Ok(registro);
        }

        // PUT: api/Registroes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRegistro([FromRoute] string id, [FromBody] Registro registro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != registro.Rut)
            {
                return BadRequest();
            }

            _context.Entry(registro).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegistroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Registroes
        [HttpPost]
        public async Task<IActionResult> PostRegistro([FromForm] string registro, IFormFile imagen)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Deserializar Json
            Registro _registro = JsonConvert.DeserializeObject<Registro>(registro);
            try
            {
                //Manejo de Excepciones
                if (string.IsNullOrEmpty(_registro.Rut))
                    throw new ArgumentException("Debe ingresar su RUT");
                if (_context.Registro.Any(x => x.Rut == _registro.Rut))
                    throw new ArgumentException("El Rut ingresado ya existe en nuestros registros");
                if (string.IsNullOrEmpty(_registro.Nombre))
                    throw new ArgumentException("Debe ingresar su Nombre");
                if (string.IsNullOrEmpty(_registro.ApPaterno))
                    throw new ArgumentException("Debe ingresar su apellido paterno");
                if (string.IsNullOrEmpty(_registro.ApMaterno))
                    throw new ArgumentException("Debe ingresar su apellido materno");
                if (string.IsNullOrEmpty(_registro.Telefono))
                    throw new ArgumentException("Debe ingresar su número telefonico");
                if (string.IsNullOrEmpty(_registro.Correo))
                    throw new ArgumentException("Debe ingresar su correo");
                if (string.IsNullOrEmpty(_registro.Password))
                    throw new ArgumentException("Debe ingresar una contraseña");
                if (string.IsNullOrEmpty(_registro.RepPassword))
                    throw new ArgumentException("Debe ingresar una nuevamente la contraseña");
                if (imagen.ContentType != "image/jpeg")
                    throw new ArgumentException("Solo se admiten imagenes en formato .jpg");
                if (imagen.Length / 1000000 > 2)
                    throw new ArgumentException("El peso de la imagen debe menor a 2Gb");

                //sacar usuario por rut
                var passUsuario = _context.Registro.FirstOrDefault(x => x.Rut.ToUpper() == registro.ToUpper());




                //guardar nombre de la imagen con un número único
                _registro.ImagenPerfil = Guid.NewGuid() + imagen.FileName;
                //guardar objeto registro en la base de datos
                _context.Registro.Add(_registro);
                await _context.SaveChangesAsync();

                //variable para obtener el registro que viene de la base de datos a partir de rut
                var registrobd = _context.Registro.FirstOrDefault(x => x.Rut == _registro.Rut);

                //variable para guardar ruta de carpeta Imagen_Perfil_Usuarios, donde quedaran todas la imagenes que se suban en el registro

                var Ruta = Path.Combine(Env.WebRootPath, "Imagen_Perfil_Usuarios");

                //validación para ver si la carpeta existe y de no existir crearla

                if (!Directory.Exists(Ruta))
                {
                    Directory.CreateDirectory(Ruta);
                }

                using (var stream = new FileStream(Path.Combine(Ruta, _registro.ImagenPerfil), FileMode.Create))
                {
                    //esperar que copie la imagen en el nuevo directorio
                    await imagen.CopyToAsync(stream);

                }

                
                return CreatedAtAction("GetRegistro", new { id = registrobd.Rut }, registrobd);

            }
            catch (Exception ex) {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);

            }

           
        }

        // DELETE: api/Registroes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRegistro([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var registro = await _context.Registro.FindAsync(id);
            if (registro == null)
            {
                return NotFound();
            }

            _context.Registro.Remove(registro);
            await _context.SaveChangesAsync();

            return Ok(registro);
        }

        private bool RegistroExists(string id)
        {
            return _context.Registro.Any(e => e.Rut == id);
        }
    }
}
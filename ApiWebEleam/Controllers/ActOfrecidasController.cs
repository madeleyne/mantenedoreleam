﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiWebEleam.Models;
using Newtonsoft.Json;

namespace ApiWebEleam.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActOfrecidasController : ControllerBase
    {
        private readonly eleamcruddbContext _context;

        public ActOfrecidasController(eleamcruddbContext context)
        {
            _context = context;
        }

        // GET: api/ActOfrecidas
        [HttpGet]
        public IEnumerable<ActOfrecida> GetActOfrecida()
        {
            return _context.ActOfrecida;
        }

        // GET: api/ActOfrecidas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetActOfrecida([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actOfrecida = await _context.ActOfrecida.FindAsync(id);

            if (actOfrecida == null)
            {
                return NotFound();
            }

            return Ok(actOfrecida);
        }

        // PUT: api/ActOfrecidas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActOfrecida([FromRoute] int id, [FromBody] ActOfrecida actOfrecida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actOfrecida.IdactOfrecida)
            {
                return BadRequest();
            }

            _context.Entry(actOfrecida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActOfrecidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ActOfrecidas
        [HttpPost]
        public async Task<IActionResult> PostActOfrecida([FromForm] string actOfrecida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Deserializar JSON
            ActOfrecida _actOfrecida = JsonConvert.DeserializeObject<ActOfrecida>(actOfrecida);

            _context.ActOfrecida.Add(_actOfrecida);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActOfrecida", new { id = _actOfrecida.IdactOfrecida }, actOfrecida);
        }

        // DELETE: api/ActOfrecidas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActOfrecida([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actOfrecida = await _context.ActOfrecida.FindAsync(id);
            if (actOfrecida == null)
            {
                return NotFound();
            }

            _context.ActOfrecida.Remove(actOfrecida);
            await _context.SaveChangesAsync();

            return Ok(actOfrecida);
        }

        private bool ActOfrecidaExists(int id)
        {
            return _context.ActOfrecida.Any(e => e.IdactOfrecida == id);
        }
    }
}
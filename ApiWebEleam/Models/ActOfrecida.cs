﻿using System;
using System.Collections.Generic;

namespace ApiWebEleam.Models
{
    public partial class ActOfrecida
    {
        public int IdactOfrecida { get; set; }
        public string NombreActividadOfrecida { get; set; }
        public string DescripActividadOfrecida { get; set; }
        public string AprobarActOfrecida { get; set; }
    }
}

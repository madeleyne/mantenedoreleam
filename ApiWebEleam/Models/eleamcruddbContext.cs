﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApiWebEleam.Models
{
    public partial class eleamcruddbContext : DbContext
    {
        public eleamcruddbContext()
        {
        }

        public eleamcruddbContext(DbContextOptions<eleamcruddbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Actividades> Actividades { get; set; }
        public virtual DbSet<ActOfrecida> ActOfrecida { get; set; }
        public virtual DbSet<Registro> Registro { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Actividades>(entity =>
            {
                entity.HasKey(e => e.Idactividades);

                entity.ToTable("actividades");

                entity.Property(e => e.Idactividades)
                    .HasColumnName("idactividades")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Descripactividad)
                    .IsRequired()
                    .HasColumnName("descripActividad")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Imagenactividad)
                    .HasColumnName("imagenActividad")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Nombreactividad)
                    .IsRequired()
                    .HasColumnName("nombreActividad")
                    .HasColumnType("varchar(45)");
            });

            modelBuilder.Entity<ActOfrecida>(entity =>
            {
                entity.HasKey(e => e.IdactOfrecida);

                entity.ToTable("act_ofrecida");

                entity.Property(e => e.IdactOfrecida)
                    .HasColumnName("idact_Ofrecida")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AprobarActOfrecida)
                    .HasColumnName("aprobar_act_ofrecida")
                    .HasColumnType("char(2)");

                entity.Property(e => e.DescripActividadOfrecida)
                    .HasColumnName("descrip_act_ofrecida")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.NombreActividadOfrecida)
                    .IsRequired()
                    .HasColumnName("nombre_act_ofrecida")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Registro>(entity =>
            {
                entity.HasKey(e => e.Rut);

                entity.ToTable("registro");

                entity.HasIndex(e => e.Rut)
                    .HasName("rut_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Rut)
                    .HasColumnName("rut")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.ApMaterno)
                    .IsRequired()
                    .HasColumnName("apMaterno")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ApPaterno)
                    .IsRequired()
                    .HasColumnName("apPaterno")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasColumnName("correo")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ImagenPerfil)
                    .HasColumnName("imagenPerfil")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasColumnName("telefono")
                    .HasColumnType("varchar(9)");
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ApiWebEleam.Models
{
    public partial class Actividades
    {
        public int Idactividades { get; set; }
        public string Nombreactividad { get; set; }
        public string Descripactividad { get; set; }
        public string Imagenactividad { get; set; }
    }
}
